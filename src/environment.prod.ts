export type Environment = {
  production: boolean,
  topics: string[],
  api: string,
  appName: string
}

export const environment: Environment = {
  production: true,
  appName: 'BLOGGER',
  api: "http://server:8080/api/v1",
  topics: [
    "Technology",
    "Travel",
    "Health & Wellness",
    "Fashion",
    "Food & Cooking",
    "Sports",
    "Business",
    "Personal Development",
    "Entertainment",
    "Lifestyle",
    "Finance",
    "Education",
    "Home Decor",
    "Parenting",
    "Beauty",
    "Gaming",
    "Art & Design",
    "Science",
    "Books",
    "Photography"
  ]
}
