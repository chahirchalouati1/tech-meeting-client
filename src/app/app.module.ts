import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ImagePipe } from './common/pipes/image.pipe';
import {HttpClientModule} from "@angular/common/http";
import { CardComponent } from './card/card.component';
import { HeadingComponent } from './heading/heading.component';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ImagePipe,
    CardComponent,
    HeadingComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
