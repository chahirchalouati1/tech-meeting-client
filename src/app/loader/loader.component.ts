import {Component} from '@angular/core';
import {Environment, environment} from "../../environment";

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {
  environment: Environment = environment;
}
