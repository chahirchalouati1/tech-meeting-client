import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { map } from 'rxjs/operators';

@Pipe({
  name: 'image'
})
export class ImagePipe implements PipeTransform {

  constructor(private httpClient: HttpClient) {}

  transform(imageUrl: string): any {
    return this.httpClient.get(imageUrl, { responseType: 'blob' })
      .pipe(map((imageBlob: Blob) => URL.createObjectURL(imageBlob)));
  }
}
