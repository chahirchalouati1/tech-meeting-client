import { Component } from '@angular/core';
import {environment} from "../../../environment";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  isMenuOpen: boolean=false;
  isMobileMenuOpen: boolean=false;
  protected readonly environment = environment;
}
